package sample;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.stage.FileChooser;
import org.omg.PortableInterceptor.SYSTEM_EXCEPTION;

import java.io.*;
import java.net.URL;
import java.util.Observable;
import java.util.ResourceBundle;

public class Controller implements Initializable {

    public GridPane mycontroller;
    @FXML
    public DatePicker brithday;
    public RadioButton rbM;
    public RadioButton rbG;
    public RadioButton rbFree;
    public ToggleGroup SexGroup;
    public ImageView myAvatar;
    public Button btnSelectAvater;
    public ComboBox <String> cbblood;
    public ComboBox <String> cbplace;


    public void doSelectBrithday(ActionEvent actionEvent) {
        System.out.println(brithday.getValue());
    }


    public void doSelectSex(ActionEvent actionEvent) {
        RadioButton radioButton = (RadioButton)actionEvent.getSource();
        System.out.println(radioButton.getText());

    }

    public void doSelectAvatar(ActionEvent actionEvent) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open Resource File");
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("影像檔","*.png","*jpg","*gif"));
        File selectedFile = fileChooser.showOpenDialog(mycontroller.getScene().getWindow());
        System.out.println(selectedFile.getAbsoluteFile());

        myAvatar.setImage(new Image(selectedFile.toURI().toString()));

    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        ObservableList<String> items = FXCollections.observableArrayList();
        items.addAll("A","O","B","P");
        cbblood.getItems().addAll(items);


        ObservableList<String> itemplaces = FXCollections.observableArrayList();
        String filename = "C:\\Users\\CSIE-E518\\Desktop\\place.txt";
        String line;
        try{
            BufferedReader bufferedReader = new BufferedReader(
                    new InputStreamReader(
                            new FileInputStream(filename)));

            while ((line=bufferedReader.readLine())!=null){
                itemplaces.add(line);
            }
            bufferedReader.close();
            cbplace.setItems(itemplaces);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
